#!/usr/bin/env node

/**
 * Include de environment variables
 */

// require('@glimpse/glimpse').init();
require('babel-register');
require('dotenv').config();
/**
 * Module dependencies.
 */

const app = require('../app');
const debug = require('debug')('magic-box:server');
const http = require('http');
const config = require('../server/config/config.js');
const welcomeMessage = require('../server/scripts/welcomeMessage');
const ip = require('ip');
const chalk = require('chalk');
/**
 * Get port from environment and store in Express.
 */

const port = config.get('server.port');
app.set('port', port);

/**
 * Create HTTP server.
 */

const server = http.createServer(app);

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  const bind = typeof port === 'string' ? `Pipe ${port}` : `Port ${port}`;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(`${bind} requires elevated privileges`);
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(`${bind} is already in use`);
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  const addr = server.address();
  const bind = typeof addr === 'string' ? `pipe ${addr}` : `port ${addr.port}`;
  console.log(welcomeMessage);
  console.log(
    chalk.green(`Server started at`, chalk.blue(`${ip.address()}:${addr.port}`))
  );
  console.log(
    `${chalk.green('Environment: ')} ${chalk.blue(process.env.NODE_ENV)}`
  );
  debug(`Listening on ${bind}`);
}

/**
 * Listen on provided port, on all network interfaces.
 */
server.listen(port);
server.on('error', onError);
server.on('listening', onListening);
