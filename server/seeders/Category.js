const categories = [
  {
    id: '8d284034-c1f6-45af-b094-0c815e7815f5',
    sortOrder: 1,
    title: 'Comedy',
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    id: '8d284034-c1f6-45af-b094-0c815e7815f6',
    sortOrder: 2,
    title: 'Animation',
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    id: '8d284034-c1f6-45af-b094-0c815e7815f7',
    sortOrder: 3,
    title: 'Breakfast',
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    id: '8d284034-c1f6-45af-b094-0c815e7815f8',
    sortOrder: 4,
    title: 'Diverse',
    createdAt: new Date(),
    updatedAt: new Date()
  }
];

module.exports = {
  up: queryInterface =>
    queryInterface
      .bulkInsert(
        'Categories',
        categories.map(ent => ({
          id: ent.id,
          sortOrder: ent.sortOrder,
          createdAt: ent.createdAt,
          updatedAt: ent.updatedAt
        }))
      )
      .then(() =>
        queryInterface.bulkInsert(
          'Category_i18ns',
          categories.map(ent => ({
            language_id: 'EN',
            parent_id: ent.id,
            title: ent.title
          }))
        )
      ),

  down: queryInterface => queryInterface.bulkDelete('Categories', null, {})
};
