import { Router } from 'express';

const users = require('./user');

const router = Router();

/* GET home page */
router.get('/', (req, res) => {
	res.redirect('/glimpse/client');
});

router.use('/users', users);

module.exports = router;
