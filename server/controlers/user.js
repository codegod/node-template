import Router from 'express';

// =============================================================================
//                            Services
// =============================================================================

import * as mailer from '../services/mailer';
import * as httpErrors from '../services/http-errors';

// =============================================================================
//                                Models
// =============================================================================

import { User } from '../models';

const router = Router();

router
	.route('/')
	// =================== GET ALL users =========================================
	.get((req, res, next) => {
		//get
	})
	// ========================= Account Registration =============================
	.post((req, res, next) => {
		// post
	});

module.exports = router;
