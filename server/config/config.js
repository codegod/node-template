const convict = require('convict');
const schema = require('./schema.js');

const config = convict(schema);
const env = config.get('env');

config.loadFile([
  `${__dirname}/common.json`,
  `${__dirname}/config_${env}.json`,
]);

config.validate({ allowed: 'strict' });

module.exports = config;
