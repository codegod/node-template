import { Strategy as JwtStrategy, ExtractJwt } from 'passport-jwt';
import dotenv from 'dotenv';
import chalk from 'chalk';
import { User } from '../models';

dotenv.config();

module.exports = passport => {
	const opts = {};
	opts.jwtFromRequest = ExtractJwt.fromAuthHeader();
	console.log(process.env.SECRET);
	opts.secretOrKey = process.env.SECRET;
	passport.use(
		new JwtStrategy(opts, (jwtPayload, done) => {
			User.findById(jwtPayload.id)
				.then(authUser => {
					if (authUser) {
						const user = {};
						user.id = authUser.id;
						done(null, user);
					} else done(null, false);
				})
				.catch(error => console.log(chalk.red(error)));
		})
	);
};
