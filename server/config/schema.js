module.exports = {
  env: {
    doc: 'The applicaton environment.',
    format: ['production', 'development', 'test'],
    default: 'development',
    env: 'NODE_ENV',
  },

  database: {
    name: {
      doc: 'Database name ',
      format: String,
      default: 'tvbox',
      env: 'DB_NAME',
    },
    username: {
      doc: 'Database user name',
      format: String,
      default: 'postgres',
      env: 'DB_USERNAME',
    },
    password: {
      doc: 'Database user password',
      format: String,
      default: 'null',
      env: 'DB_PASSWORD',
    },
    options: {
      host: {
        doc: 'Database user password',
        format: 'url',
        default: 'null',
        env: 'DB_HOST',
      },
      dialect: {
        doc: 'Database user password',
        format: String,
        default: 'null',
        env: 'DB_DIALECT',
      },
      logging: {
        doc: 'Database user password',
        format: '*',
        default: console.log,
      },
    },
  },
  server: {
    port: {
      doc: 'The port to bind.',
      format: 'port',
      default: 8000,
      env: 'SERVER_PORT',
      arg: 'port',
    },
    secret: {
      doc: 'Server Secret',
      format: '*',
      default: 10,
      env: 'SECRET',
    },
    tokenValidTime: {
      doc: 'How many seconds token will be active',
      format: Number,
      default: 604800,
      env: 'TOKENEXPIRETIME',
    },
  },
  email: {
    username: {
      doc: 'The port to bind.',
      format: '*',
      default: 'starnetbox@gmail.com',
      env: 'GMAIL_SMTP_USERNAME',
    },
    password: {
      doc: 'Server Secret',
      format: '*',
      default: '91*7wD9RjH5a',
      env: 'GMAIL_SMTP_PASSWORD',
    },
    tokenValidTime: {
      doc: 'How many minutes token will be active',
      format: Number,
      default: 120,
      env: 'EMAIL_TOKEN_VALID_TIME',
    },
    links: {
      confirmation: {
        doc: 'Link for account confirmation via email',
        format: 'url',
        default: 'http://192.168.70.254:8000/users/confirm/',
        env: 'EMAIL_CONFIRMATION_LINK',
      },
      resetPassword: {
        doc: 'Link to genereate reset password proccedure',
        format: 'url',
        default: 'http://192.168.70.254:8000/users/resetpassword/',
        env: 'EMAIL_RESETPASWORD_LINK',
      },
    },
  },
  languages: {
    list: {
      doc: 'List of languages',
      format: 'Array',
      default: ['EN', 'RO', 'RU', 'IT', 'FR', 'DE'],
    },
    default: {
      doc: 'Default languages',
      format: 'Array',
      default: 'EN',
    },
  },
};
