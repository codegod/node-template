const HttpError = require('http-error-constructor');

module.exports.userNotFound = new HttpError(404, 'User not Found');
