import config from '../config/config';

const nodeMailer = require('nodemailer');
const hbs = require('nodemailer-express-handlebars');
/* eslint-disable no-console */
const user = config.get('email.username');
const pass = config.get('email.password');
const confirmationLink = config.get('email.links.confirmation');
const resetPasswordLink = config.get('email.links.resetPassword');

const smtpTransport = nodeMailer.createTransport({
	host: 'smtp.gmail.com',
	port: 465,
	secure: true, // upgrade later with STARTTLS
	auth: {
		user,
		pass
	}
});

smtpTransport.use(
	'compile',
	hbs({ viewPath: `${__dirname}/../resources/templates/` })
);

smtpTransport
	.verify()
	.then(() => console.log('Email Server is Ready'))
	.catch(error => console.log('Email Server ', error));

function getConfermationEmailTemplate(recipientMail, name, surname) {
	return {
		from: '"test" <no-reply@test.md>',
		to: recipientMail,
		subject: 'subj',
		template: 'confirmation',
		context: {
			name,
			surname
		}
	};
}

module.exports.sendConfirmationEmail = (recipientMail, name, surname) => {
	const template = getConfermationEmailTemplate(recipientMail, name, surname);
	console.log(template);
	return smtpTransport.sendMail(template);
};
