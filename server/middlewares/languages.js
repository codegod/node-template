import config from '../config/config';

const languagesList = config.get('languages.list');
const defaultLang = config.get('languages.default');

const acceptLanguage = (req, res, next) => {
	let lang = req.headers['accept-language'];
	if (lang) lang = lang.toUpperCase();
	req.lang = languagesList.includes(lang) ? lang : defaultLang;
	next();
};

module.exports = acceptLanguage;
