// =============================================================================
//                                Models
// =============================================================================
import { User } from '../models';
// =============================================================================
//                                Services
// =============================================================================
import * as httpErrors from '../services/http-errors';

function guard(alias) {
  return (req, res, next) => {
    if (!alias) throw httpErrors.noAlias;
    if (!req.user) throw httpErrors.userNotAuthenicated;
    if (/* Logic for allow access */) {
      next()
    } else {
      next(/* Error access denied */)
    }
  };
}
export default guard;
