const expect = require('chai').expect;

describe('Enviroment Variables', () => {
  before(() => {
    require('dotenv').config();
  });

  describe('Server Port', () => {
    it('should be a number', () => {
      const serverPort = parseInt(process.env.SERVER_PORT, 10);
      expect(serverPort).to.be.a('number');
    });
  });
});
