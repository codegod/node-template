// const execa = require('execa');
const Listr = require('listr');

const tasks = new Listr([
  {
    title: 'Success',
    task: () => 'Foo',
  },
]);

tasks.run().catch(err => {
  console.error(err);
});
