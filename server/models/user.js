import bcrypt from 'bcrypt';
import { Model } from 'sequelize';

export default (sequelize, DataTypes) => {
	class User extends Model {
		static associate(models) {
			// User.hasOne(models.Serviceprovider, {
			// 	foreignKey: 'userId'
			// });
		}

		confirmEmail() {
			this.isEmailconfirmed = true;
		}

		changePassword(password) {
			// Get number of Rounds for bcrypt or put default 10
			const saltRounds = parseInt(process.env.SALT_ROUNDS, 10) || 10;

			// Generate Hash of plain passwod using bcrypt
			return bcrypt.hash(password, saltRounds).then(hasedPassword => {
				this.password = hasedPassword;
				// Replace user plain text password with hashed
				return this.save();
			});
		}

		comparePasswords(password) {
			return bcrypt.compareSync(password, this.password);
		}
	}
	User.init(
		{
			id: {
				type: DataTypes.UUID,
				defaultValue: DataTypes.UUIDV4,
				primaryKey: true,
				allowNull: false
			},
			email: {
				type: DataTypes.STRING,
				allowNull: false,
				unique: {
					msg: 'Email Allready Exist'
				},
				validate: {
					isEmail: {
						msg: 'Wrong email format'
					}
				}
			},
			password: {
				type: DataTypes.STRING,
				allowNull: false,
				validate: {
					notEmpty: true
				}
			}
		},
		{
			sequelize,
			hooks: {
				beforeCreate: user => {
					// Get number of Rounds for bcrypt or put default 10
					const saltRounds = parseInt(process.env.SALT_ROUNDS, 10) || 10;

					// Generate Hash of plain passwod using bcrypt
					return bcrypt.hash(user.password, saltRounds).then(hasedPassword => {
						user.password = hasedPassword; // Replase user plain text password with hashed
					});
				}
			},
			onDelete: 'CASCADE'
		}
	);

	return User;
};
