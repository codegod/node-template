import chalk from 'chalk';
import config from '../config/config';

const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
const SequelizeI18N = require('sequelize-i18n');

const basename = path.basename(module.filename);
const db = {};

const sequelize = new Sequelize(
  config.get('database.name'),
  config.get('database.username'),
  config.get('database.password'),
  config.get('database.options')
);

const i18n = new SequelizeI18N(sequelize, {
  languages: config.get('languages.list'),
  default_language: config.get('languages.default')
});
i18n.init();

fs
  .readdirSync(__dirname)
  .filter(
    file =>
      file.indexOf('.') !== 0 && file !== basename && file.slice(-3) === '.js'
  )
  .forEach(file => {
    const model = sequelize.import(path.join(__dirname, file));
    db[model.name] = model;
  });

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

// Check if env is in dev mode to force sync
const isDevelopemnt = process.env.NODE_ENV === 'development';
db.sequelize
  .sync({
    force: isDevelopemnt
  })
  .then(() => {
    console.log(chalk.green('Sequelize:'), chalk.blue('Syncronization done'));
  })
  .catch(e => {
    throw new Error(e);
  });

module.exports = db;
