const express = require('express');
const logger = require('morgan');
const bodyParser = require('body-parser');
const passport = require('passport');
const cors = require('cors');

//  Import Controllers
const index = require('./server/controlers/index');

const app = express();

// Middleware
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: false
  })
);
app.use(cors());

// Include passport
app.use(passport.initialize());
require('./server/config/passport')(passport);

// Routes
app.use(index);

// error handler
app.use((error, req, res, next) => {
  // set locals, only providing error in development
  res.locals.message = error.message;
  res.locals.error = req.app.get('env') === 'development' ? error : {};

  switch (error.name) {
    case 'SequelizeDatabaseError':
      res.status(error.status || 400).json(error.message);
      break;

    case 'SequelizeValidationError':
      res.status(error.status || 409).json(error.message);
      break;

    default:
      res.status(error.status || 400).send(error.message);
  }
  next();
  console.log(error);
});

module.exports = app;
